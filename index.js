//npm install express --save
import express from 'express'
import itemRoutes3 from './src/items3.js'
import itemRoutes2 from './src/items2.js'
import itemRoutes from './src/items.js'
const app = express()
const port = 3000
// allows us to parse json 
app.use(express.json())

app.get('/', (req, res) => res.send('Hello efreetr world 9'))

app.use('/items', itemRoutes)
app.use('/items2', itemRoutes2)
app.use('/items3', itemRoutes3)


app.listen(port, () => console.log(`API server ready on http://localhost:${port}`))
//test